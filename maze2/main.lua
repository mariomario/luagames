-- main.lua

-- Maze data
local maze = {
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1},
    {1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1},
    {1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1},
    {1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1},
    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},    
    {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
}


-- Character position
local characterX = 7*64
local characterY = 9*64

-- Maze tile size
local tileSize = 64

-- Load assets
function love.load()
    love.window.setMode(800, 800) 
    character = love.graphics.newImage("character.png")
    wall = love.graphics.newImage("wall.png")
    goal = love.graphics.newImage("goal.png")
end

-- Update game state
function love.update(dt)
    if love.keyboard.isDown("up") then
        if maze[math.floor((characterY - 1) / tileSize) + 1][math.floor(characterX / tileSize) + 1] == 0 then
            characterY = characterY - 64
        end
    elseif love.keyboard.isDown("down") then
        if maze[math.floor((characterY + tileSize) / tileSize) + 1][math.floor(characterX / tileSize) + 1] == 0 then
            characterY = characterY + 64
        end
    end

    if love.keyboard.isDown("left") then
        if maze[math.floor(characterY / tileSize) + 1][math.floor((characterX - 1) / tileSize) + 1] == 0 then
            characterX = characterX - 64
        end
    elseif love.keyboard.isDown("right") then
        if maze[math.floor(characterY / tileSize) + 1][math.floor((characterX + tileSize) / tileSize) + 1] == 0 then
            characterX = characterX + 64
        end
    end
end

-- Draw game objects
function love.draw()
    for i = 1, #maze do
        for j = 1, #maze[i] do
            if maze[i][j] == 1 then
                love.graphics.draw(wall, (j - 1) * tileSize, (i - 1) * tileSize)
            else
                love.graphics.draw(goal, (j - 1) * tileSize, (i - 1) * tileSize)
            end
        end
    end

    love.graphics.draw(character, characterX, characterY)
end
