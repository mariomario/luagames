function fillBorder(matrix)
  local numRows = #matrix
  local numCols = #matrix[1]

  -- Set first row to 1s
  for j = 1, numCols do
    matrix[1][j] = 1
  end

  -- Set first column to 1s
  for i = 2, numRows do
    matrix[i][1] = 1
  end

  -- Set last column to 1s
  for i = 1, numRows do
    matrix[i][numCols] = 1
  end

  -- Set last row to 1s
  for j = 1, numCols-1 do
    matrix[numRows][j] = 1
  end

  return matrix
end


function generateRandomMatrix(n)
  local matrix = {}
  for i=1,n do
    matrix[i] = {}
    for j=1,n do
      matrix[i][j] = 0
    end
  end

  return matrix
end

function love.load()
  -- Set up the maze and player variables
  maze = fillBorder(generateRandomMatrix(15))
  player = {
    x = 2,
    y = 2
  }
end

function love.update(dt)
  -- Move the player based on keyboard input
  if love.keyboard.isDown("up") then
    if maze[player.y - 1][player.x] == 0 then
      player.y = player.y - 1
    end
  elseif love.keyboard.isDown("down") then
    if maze[player.y + 1][player.x] == 0 then
      player.y = player.y + 1
    end
  elseif love.keyboard.isDown("left") then
    if maze[player.y][player.x - 1] == 0 then
      player.x = player.x - 1
    end
  elseif love.keyboard.isDown("right") then
    if maze[player.y][player.x + 1] == 0 then
      player.x = player.x + 1
    end
  end
  love.timer.sleep(0.05) 
end

rx = 10
ry = 10
rx2 = 7
ry2 = 4
hovinto = 32

function sign(num)
  if num > 0 then
    return 1
  elseif num < 0 then
    return -1
  else
    return 0
  end
end

function love.draw()
  u = math.random(0, 30)
  if u < 3 then
    rx = rx - sign(player.x - rx2)
    ry = ry - sign(player.y - ry2)
    if rx > 14 then
        rx = 14
    end
    if ry > 14 then
        ry = 14
    end
    if rx < 1 then
        rx = 1
    end
    if ry < 1 then
        ry = 1
    end
  end
  if u < 2 then
    rx2 = rx2 + sign(player.x - rx2)
    ry2 = ry2 + sign(player.y - ry2)
  end
  -- Draw the maze and player
  love.graphics.setColor(1.0, 1.0, 1.0)
  for y=1,#maze do
    for x=1,#maze[y] do
      if maze[y][x] == 1 then
        love.graphics.rectangle("fill", x*32, y*32, 32, 32)
      end
      if x==rx and y==ry then
        love.graphics.setColor(0.0, 0.7, 0.1)
        love.graphics.rectangle("fill", x*32, y*32, 32, 32)
        love.graphics.setColor(1, 1, 1) -- set color back to white
      end
      if x==rx2 and y==ry2 then
        love.graphics.setColor(0.0, 0.1, 0.7)
        love.graphics.rectangle("fill", x*32, y*32, 32, 32)
        love.graphics.setColor(1, 1, 1) -- set color back to white
      end
    end
  end
  love.graphics.setColor(1, 0.4, 0.4)
  love.graphics.print("MARCO", 4, 3)
  love.graphics.print("ANTONIO", 60, 3)
  t = os.clock()
  love.graphics.print(string.format("%.2f\n", t), 300, 3)
  if (((t > 100) or (hovinto == 11) or (player.x == rx2 and player.y == ry2)) and ((hovinto == 32) or (hovinto==11))) then
    love.graphics.setColor(0.0, 0.0, 0.0)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    love.graphics.setColor(1.0, 1.0, 0.0)
    love.graphics.print("TU PERDI", 100, 100)
    hovinto = 11
  end
  love.graphics.rectangle("fill", player.x*32, player.y*32, 32, 32)
  if ((player.x == rx and player.y == ry) or (hovinto == -4))then
    love.graphics.setColor(0.0, 0.0, 0.0)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    love.graphics.setColor(1.0, 1.0, 0.0)
    love.graphics.print("TU VINCI", 100, 100)
    hovinto = -4
  end
end

