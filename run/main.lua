-- Constants
local SCREEN_WIDTH = 1400
local SCREEN_HEIGHT = 200
local PLAYER_RADIUS = 20
local PLAYER_SPEED = 200
local FINISH_LINE = SCREEN_WIDTH - PLAYER_RADIUS

-- Player 1 state
local player1 = {
    x = 0,
    y = SCREEN_HEIGHT / 2,
    speed = 0
}

-- Player 2 state
local player2 = {
    x = 0,
    y = SCREEN_HEIGHT / 2 + 50,
    speed = 0
}

-- Load game
function love.load()
    love.window.setTitle("Racing Game")
    love.window.setMode(SCREEN_WIDTH, SCREEN_HEIGHT)
end

function f(x)
    if (x == 0) then
        return 0
    end
    return math.exp(-1.5 + math.cos(x))
end

-- Update game
function love.update(dt)
  -- Player 1
  if love.keyboard.isDown("a") then
    player1.timer = player1.timer + dt  -- Increment the timer for player 1
  else
    player1.timer = 0  -- Reset the timer for player 1 if 'a' key is not pressed
  end

  -- Calculate the speed for player 1 based on the timer
  player1.speed = PLAYER_SPEED * f(player1.timer)

  -- Update player 1's position based on the speed
  player1.x = player1.x + player1.speed * dt

  -- Player 2
  if love.keyboard.isDown("l") then
    player2.timer = player2.timer + dt  -- Increment the timer for player 2
  else
    player2.timer = 0  -- Reset the timer for player 2 if 'l' key is not pressed
  end

  -- Calculate the speed for player 2 based on the timer
  player2.speed = PLAYER_SPEED * f(player2.timer)

  -- Update player 2's position based on the speed
  player2.x = player2.x + player2.speed * dt

    -- Check if players reach the finish line
    if player1.x >= FINISH_LINE or player2.x >= FINISH_LINE then
        local winner = player1.x >= FINISH_LINE and "Player 1" or "Player 2"
        print(winner .. " wins!")
        love.event.quit()
    end
end

-- Draw game
function love.draw()
    -- Draw players
    love.graphics.setColor(49/255, 216/255, 67/255) -- Player 1 color 
    love.graphics.circle("fill", player1.x, player1.y, PLAYER_RADIUS)

    love.graphics.setColor(1, 198/255, 0) -- Player 2 color (blue)
    love.graphics.circle("fill", player2.x, player2.y, PLAYER_RADIUS)

    -- Draw finish line
    love.graphics.setColor(1, 1, 1) -- Finish line color (white)
    love.graphics.line(FINISH_LINE, 0, FINISH_LINE, SCREEN_HEIGHT)

    -- Print instructions
    love.graphics.setColor(1, 1, 1)
    love.graphics.print("Player 1: Press 'a' repeatedly to run", 10, 10)
    love.graphics.print("Player 2: Press 'l' repeatedly to run", 10, 30)
end
