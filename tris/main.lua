-- Tris game in Lua using LÖVE framework

-- Load LÖVE library
love.graphics = require("love.graphics")
love.window = require("love.window")
love.timer = require("love.timer")

-- Initialize game state
local g = {{" ", " ", " "},{" ", " ", " "},{" ", " ", " "}}
local currentPlayer = "X"

-- Load assets
function love.load()
    love.window.setTitle("Tris")
    love.window.setMode(300, 300)
end

-- Draw game board
function love.draw()
    local tileSize = 100
    for i=1,3 do
        for j=1,3 do
            love.graphics.rectangle("line", (j-1)*tileSize, (i-1)*tileSize, tileSize, tileSize)
            love.graphics.print(g[i][j], (j-1)*tileSize + 40, (i-1)*tileSize + 40, 0, 2, 2)
        end
    end
end

-- Check for win or draw
function checkWin(player)
    for i=1,3 do
        if g[i][1] == player and g[i][2] == player and g[i][3] == player then
            return true
        end
    end
    for i=1,3 do
        if g[1][i] == player and g[2][i] == player and g[3][i] == player then
            return true
        end
    end
    if g[1][1] == player and g[2][2] == player and g[3][3] == player then
        return true
    end
    if g[1][3] == player and g[2][2] == player and g[3][1] == player then
        return true
    end
    return false
end

-- Check for draw
function isBoardFull()
    for i=1,3 do
        for j=1,3 do
            if g[i][j] == " " then
                return false
            end
        end
    end
    return true
end

-- Make a move
function makeMove(player, row, col)
    if g[row][col] == " " then
        g[row][col] = player
        return true
    else
        return false
    end
end

-- Handle mouse click event
function love.mousepressed(x, y, button, istouch)
    if button == 1 then
        local tileSize = 100
        local row = math.floor(y / tileSize) + 1
        local col = math.floor(x / tileSize) + 1
        if row >= 1 and row <= 3 and col >= 1 and col <= 3 then
            if makeMove(currentPlayer, row, col) then
                if currentPlayer == "X" then
                    love.graphics.setColor(0.5, 0, 0)
                    currentPlayer = "O"
                else
                    love.graphics.setColor(0.1, 0.6, 0.9)
                    currentPlayer = "X"
                end
            end
        end
    end
end
