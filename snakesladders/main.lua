local BOARD_WIDTH = 20 -- Board width (number of tiles)
local BOARD_HEIGHT = 10 -- Board height (number of tiles)
local TILE_SIZE = 40 -- Size of each tile in pixels
local PLAYER_SIZE = 15 -- Size of the player character in pixels

local players = {} -- Table to store player positions
local currentPlayer = 1 -- Index of the current player
local dieValue = 1 -- Value of the rolled die

-- Define player colors
local playerColors = {
    {1, 0, 0, 0.5}, -- Player 1 color (red)
    {0, 0, 1, 0.5}  -- Player 2 color (blue)
}

-- Initialize players
for i = 1, 2 do
    players[i] = {x = 1, y = 1} 
end

gameEnded = 0

-- Update player positions
function updatePlayerPositions(playerIndex)
    local player = players[playerIndex]
    player.x = player.x + dieValue
    if player.x > BOARD_WIDTH then
        player.x = player.x - BOARD_WIDTH
        player.y = player.y + 1
    end
    if player.y > BOARD_HEIGHT then
        gameEnded = 1
    end
end

-- Handle keypress events
function love.keypressed(key)
    if key == "return" then
        -- Roll the die
        dieValue = love.math.random(1, 6)
        updatePlayerPositions(currentPlayer)
        currentPlayer = currentPlayer % 2 + 1 -- Switch to the other player
    end
end

function love.load()
  love.window.setTitle("Marchik dice game")
  love.graphics.setBackgroundColor(247/255, 231/255, 206/255) -- Set background color to champagne
end

-- Draw the game
function love.draw()
    if (gameEnded == 0) then
        love.graphics.setColor(0.2, 0.2, 0.2) -- Dark gray color for tiles

        -- Draw board tiles
        for x = 1, BOARD_WIDTH do
            for y = 1, BOARD_HEIGHT do
                local posX = (x - 1) * TILE_SIZE
                local posY = (y - 1) * TILE_SIZE
                love.graphics.rectangle("line", posX, posY, TILE_SIZE, TILE_SIZE)
            end
        end

        -- Draw players
        for i = 1, 2 do
            local player = players[i]
            local posX = (player.x - 1) * TILE_SIZE + (TILE_SIZE - PLAYER_SIZE) / 2
            local posY = (player.y - 1) * TILE_SIZE + (TILE_SIZE - PLAYER_SIZE) / 2
            love.graphics.setColor(playerColors[i]) -- Set player color
            local offset = 0
            if ((players[1].x == players[2].x) and (players[1].y == players[2].y)) then
                offset = PLAYER_SIZE*(i-1) - 0.5*PLAYER_SIZE
            end
            love.graphics.rectangle("fill", posX, posY + offset, PLAYER_SIZE, PLAYER_SIZE)
        end

        -- Draw die value
        love.graphics.setColor(0.2, 0.2, 0.2) -- Reset color to white
        love.graphics.print("Die: " .. dieValue, 10, TILE_SIZE * (BOARD_HEIGHT + 1) + 10)

        -- Draw current player indicator
        love.graphics.print("Player " .. currentPlayer .. "'s turn", 10, TILE_SIZE * (BOARD_HEIGHT + 2) + 10)
    end
    if (gameEnded > 0) then
        love.graphics.setColor(0.2, 0.2, 0.2) -- Set text color to white
        love.graphics.setFont(love.graphics.newFont(48)) -- Set font size to 48
        if (gameEnded == 1) then
            currentPlayer = currentPlayer % 2 + 1 -- Switch to the other player
        end
        gameEnded = gameEnded + 1
        love.graphics.print("Player " .. currentPlayer .. " wins!", 200, 200) -- Display "You Win!" message at (200, 200)
    end
end
