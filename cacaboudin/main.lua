-- Importer la bibliothèque Love2D
function love.load()
  -- Créer un personnage
  player = {
    x = 200,
    y = 200,
    width = 32,  -- Ajouter la propriété width pour le personnage
    height = 32, -- Ajouter la propriété height pour le personnage
    speed = 100
  }

  -- Créer un objet interactif
  interactableObject = {
    x = 400,
    y = 300,
    width = 32,
    height = 32
  }

  -- Charger une police d'écriture pour afficher le message
  font = love.graphics.newFont(12)
end

function love.update(dt)
  -- Mouvement du personnage
  if love.keyboard.isDown("up") then
    player.y = player.y - player.speed * dt
  elseif love.keyboard.isDown("down") then
    player.y = player.y + player.speed * dt
  end
  if love.keyboard.isDown("left") then
    player.x = player.x - player.speed * dt
  elseif love.keyboard.isDown("right") then
    player.x = player.x + player.speed * dt
  end

  -- Vérifier si le personnage entre en contact avec l'objet interactif
  local distance = math.sqrt((player.x - interactableObject.x)^2 + (player.y - interactableObject.y)^2)
  if distance < player.width/2 + interactableObject.width/2 then
    if love.keyboard.isDown("e") then
      message = "Caca boudin!"
    end
  else
    message = nil
  end
end

function love.draw()
  -- Dessiner le personnage
  love.graphics.setColor(1, 0, 0)
  love.graphics.circle("fill", player.x, player.y, player.width/2)

  -- Dessiner l'objet interactif
  love.graphics.setColor(0, 1, 0)
  love.graphics.rectangle("fill", interactableObject.x - interactableObject.width/2, interactableObject.y - interactableObject.height/2, interactableObject.width, interactableObject.height)

  -- Afficher le message à l'écran
  if message then
    love.graphics.setColor(1, 1, 1)
    love.graphics.setFont(font)
    love.graphics.print(message, 10, 10)
  end
end
